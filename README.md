# duurzaam-baflo-rasquert-assignment
Duurzaam baflo frontend assignment year 2

In order to open this project you will need to have installed Vagrant and Virtualbox.

The passwords to the wordpress part is in the SQL:Passwords directory.
There is also an import file for the wp database in xml format located in this directory.

Once you have installed vagrant (don't use the latest version) you will have to run Vagrant up on the trellis directory.
This should setup your local device to now browse: duurzaambaflo.dev
