<?php

/**
 * Initialize Timber
 */
if (!class_exists('Timber'))
{
    add_action('admin_notices', function ()
    {
        echo '
            <div class="error">
                <p>Timber not activated. Make sure you activate the plugin in
                    <a href="' . esc_url(admin_url('plugins.php#timber')) . '">
                    ' . esc_url(admin_url('plugins.php')) . '
                    </a>
                </p>
            </div>
        ';
    });
    add_filter('template_include', function ($template)
    {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });
} else
{
    require(__DIR__ . '/class/class-duurzaam-baflo-site.php');
    $site = new DuurzaamBaflo\DuurzaamBafloSite();
}

/**
 * Enable WP4.1+ title support.
 */

add_action('after_setup_theme', 'theme_slug_setup');
function theme_slug_setup() {
   add_theme_support('title-tag');
}

/**
 * Load CSS and scripts.
 */

add_action('wp_enqueue_scripts', function ()
{
    wp_deregister_script('jquery');
    wp_deregister_script('wp-embed');
    wp_enqueue_style('screen', get_template_directory_uri() . '/style.min.css', null, null, 'screen');
    wp_enqueue_script('scripts', get_template_directory_uri() . '/scripts.min.js', null, null, true);
});

/**
 * Add image sizes.
 */

add_image_size('small', 320, 9999);
add_image_size('medium', 480, 9999);
add_image_size('medium_large', 768, 9999);
add_image_size('large', 1280, 9999);
add_image_size('extra_large', 1440, 9999);
add_image_size('extra_extra_large', 2000, 9999);

/**
 * Register navigation menus.
 */

add_action('after_setup_theme', function ()
{
    register_nav_menu('primary', 'Hoofdmenu');
});

/**
 *  Clean up dashboard
 */
function remove_dashboard_widgets() {
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

/**
 *  Clean up admin panel
 */

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('Administrator')) {
        show_admin_bar(false);
    }
}

function my_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('new-content');
    add_filter('show_admin_bar', '__return_false');
}
add_action( 'wp_before_admin_bar_render', 'my_admin_bar_render' );

function remove_menus(){
    /**
     * Don't ever show these pages
     */
    remove_menu_page( 'edit-comments.php' );                                //Comments
    remove_menu_page( 'edit.php' );                                         //Posts
    remove_menu_page( 'upload.php' );                                       //Media

    /**
     *  Don't show these pages unless the user is an administrator.
     */    if (current_user_can('Administrator')){
        remove_menu_page( 'index.php' );                                    //Dashboard
        remove_menu_page( 'plugins.php' );                                  //Plugins
        remove_menu_page( 'tools.php' );                                    //Tools
        remove_menu_page( 'admin.php?page=theme-general-settings' );        //Settings
    }
}
add_action( 'admin_menu', 'remove_menus' );


/**
 *  Change WP login logo
 */
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/login-logo.svg);
            height:200px;
            width:200px;
            background-size: 200px 200px;
            background-repeat: no-repeat;
            padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
function remove_personal_options(){
    echo '<script type="text/javascript">jQuery(document).ready(function($) {
  
$(\'form#your-profile > h2:first\').remove(); // remove the "Personal Options" title
  
$(\'form#your-profile tr.user-rich-editing-wrap\').remove(); // remove the "Visual Editor" field
  
$(\'form#your-profile tr.user-admin-color-wrap\').remove(); // remove the "Admin Color Scheme" field
  
$(\'form#your-profile tr.user-comment-shortcuts-wrap\').remove(); // remove the "Keyboard Shortcuts" field
  
$(\'form#your-profile tr.user-admin-bar-front-wrap\').remove(); // remove the "Toolbar" field
  
$(\'form#your-profile tr.user-nickname-wrap\').hide(); // Hide the "nickname" field
  
$(\'table.form-table tr.user-display-name-wrap\').remove(); // remove the “Display name publicly as” field
  
$(\'table.form-table tr.user-url-wrap\').remove();// remove the "Website" field in the "Contact Info" section
  
$(\'h2:contains("About Yourself"), h2:contains("About the user")\').remove(); // remove the "About Yourself" and "About the user" titles
  
$(\'form#your-profile tr.user-description-wrap\').remove(); // remove the "Biographical Info" field
  
$(\'form#your-profile tr.user-profile-picture\').remove(); // remove the "Profile Picture" field
  
});</script>';

}

add_action('admin_head','remove_personal_options');

/**
 * Create Sponsors custom-post-type
 */

add_action( 'init', 'createPartners' );

function createPartners() {

    register_post_type( 'Partners', array(
        'labels' => array(
            'name' => 'Partners',
            'singular_name' => 'Partner',
            'slug' => 'Partners',
        ),
        'description' => 'Partners',
        'menu_icon' => 'dashicons-networking',
        'public' => true,
        'menu_position' => 24,
        'supports' => array( 'title' )
    ));
}


/**
 * Make archive current when on single.
 */

add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );
function add_current_nav_class($classes, $item)
{

	// Getting the current post details
	global $post;

    // Getting the post type of the current post.
    if (is_singular('post') || is_404()) {
        $current_post_type_slug = substr(str_replace('/%postname%/', '', get_option('permalink_structure')), 1);
    } else {
    	$current_post_type = get_post_type_object(get_post_type($post->ID));
    	$current_post_type_slug = $current_post_type->rewrite['slug'];
    }

	// Getting the URL of the menu item
	$menu_slug = strtolower(trim($item->url));

	// If the menu item URL contains the current post types slug add the current-menu-item class
	if (strpos($menu_slug,$current_post_type_slug) !== false) {

	   $classes[] = 'current-menu-item';

	}

	// Return the corrected set of classes to be added to the menu item
	return $classes;

}
