<?php

namespace DuurzaamBaflo;

use \Timber;
use \TimberMenu;
use \TimberPost;
use \TimberSite;


class DuurzaamBafloSite extends TimberSite
{
    public function __construct()
    {
        add_filter('timber_context', array($this, 'addToContext'));
        parent::__construct();
    }

    public function addToContext($context)
    {
        $context['menu'] = new TimberMenu('primary');
        $context['post'] = new TimberPost();
        $context['options'] = get_fields('options');
        $context['archive_blog'] = get_post_type_archive_link('blog');
        $args = array(
          'post_type' => 'Partners',
          'posts_per_page' => -1,
          'orderby' => 'rand'
        );
        $context['partners'] = Timber::get_posts($args);

        return $context;
    }
}
