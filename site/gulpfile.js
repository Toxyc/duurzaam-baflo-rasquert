'use strict';

// Import general libraries.
const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const notify = require('gulp-notify');

// Import CSS libraries.
const sass = require('gulp-sass');
const neatIncludePaths = require('bourbon-neat').includePaths;
const cssnano = require('gulp-cssnano');
const autoprefixer = require('gulp-autoprefixer');

// Import JavaScript libraries.
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const gulpImports = require('gulp-imports');

// Define constants.
const themePath = 'web/app/themes/duurzaam-baflo';
const assetsPath = themePath + '/assets';
const viewsPath = themePath + '/views';
const hostName = 'http://duurzaambaflo.dev';

// Helpers.
function notifySuccess(message) {
	return notify({
		sound: false,
		title: 'Success',
		message: message
	});
}

function notifyFailure(message, error) {
	return notify.onError({
		sound: false,
		title: 'Failure',
		message: message + error
	})(error);
}

// Tasks.
gulp.task('compile-css', function() {
	return gulp.src( assetsPath + '/main.scss')
		.pipe(sass({
			// Set include paths for Bourbon and Neat.
			includePaths: neatIncludePaths
		})).on('error', sass.logError).on('error', function (error) {
			notifyFailure('Error compiling "style.scss"', error);
			this.emit('end');
		})
		.pipe(autoprefixer())
		.pipe(cssnano())
		.pipe(rename('style.min.css'))
		.pipe(gulp.dest(themePath))
		.pipe(browserSync.stream())
		.pipe(notifySuccess('Compiled "styles.min.css"'));
});

gulp.task('compile-js', function() {
	return gulp.src([
		assetsPath + '/main.js',
	])
    .pipe(gulpImports())
	.pipe(concat('scripts.min.js'))
	.pipe(uglify().on('error', function (error) {
        console.log(error);
    }))
	.pipe(gulp.dest(themePath))
	.pipe(notifySuccess('Compiled "scripts.min.js"'));
});

gulp.task('default', function() {
	browserSync.init({
		notify: false,
		proxy: hostName
	});

    gulp.watch([
        assetsPath + '/main.scss',
        assetsPath + '/blocks/**/*.scss',
        assetsPath + '/common/*.scss'
    ], ['compile-css']);
    gulp.watch([
        assetsPath + '/main.js',
        assetsPath + '/blocks/**/*.js',
        assetsPath + '/common/*.js'
    ], ['compile-js']);
	gulp.watch([
        themePath + '/*.php',
        viewsPath + '/*.twig'
    ]).on('change', browserSync.reload);
});
